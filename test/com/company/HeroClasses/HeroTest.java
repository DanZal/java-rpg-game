package com.company.HeroClasses;

import com.company.Armor;
import com.company.Hero;
import com.company.Item;
import com.company.Weapons;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.InvalidArmorException;
import util.InvalidWeaponException;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    private final Hero mage = new Mage();
    private final Hero ranger = new Ranger();
    private final Hero rogue = new Rogue();
    private final Hero warrior = new Warrior();



    /**Level tests **/
    @org.junit.jupiter.api.Test
    void makeHero_checkLevelWhenCreated_expectedLevelOne() {
        mage.makeHero();
        assertEquals(1, mage.getLevel());

        ranger.makeHero();
        assertEquals(1, ranger.getLevel());

        rogue.makeHero();
        assertEquals(1, rogue.getLevel());

        warrior.makeHero();
        assertEquals(1, warrior.getLevel());
    }

    @org.junit.jupiter.api.Test
    void levelUp_checkLevelWhenLevelledUp_expectedLevelTwo() {
        //Need to create the hero first then levelUp
        mage.makeHero();
        mage.levelUp();
        assertEquals(2, mage.getLevel());

        //If the previous tests passed, we will now this test will pass for all heroes if it
        //passes for one hero, as the levelUp method is the same for all heroes.
        warrior.makeHero();
        warrior.levelUp();
        assertEquals(2, warrior.getLevel());

    }

    /** Default attributes Test **/
    @org.junit.jupiter.api.Test
    void makeHero_checkVitalityAttributeWhenCreated_expectedCorrectValues() {
        mage.makeHero();
        assertEquals(5, mage.getVitality());

        ranger.makeHero();
        assertEquals(8, ranger.getVitality());

        rogue.makeHero();
        assertEquals(8, rogue.getVitality());

        warrior.makeHero();
        assertEquals(10, warrior.getVitality());
    }

    @org.junit.jupiter.api.Test
    void makeHero_checkStrengthAttributeWhenCreated_expectedCorrectValues() {
        mage.makeHero();
        assertEquals(1, mage.getStrength());

        ranger.makeHero();
        assertEquals(1, ranger.getStrength());

        rogue.makeHero();
        assertEquals(2, rogue.getStrength());

        warrior.makeHero();
        assertEquals(5, warrior.getStrength());
    }

    @org.junit.jupiter.api.Test
    void makeHero_checkDexterityAttributeWhenCreated_expectedCorrectValues() {
        mage.makeHero();
        assertEquals(1, mage.getDexterity());

        ranger.makeHero();
        assertEquals(7, ranger.getDexterity());

        rogue.makeHero();
        assertEquals(6, rogue.getDexterity());

        warrior.makeHero();
        assertEquals(2, warrior.getDexterity());
    }

    @org.junit.jupiter.api.Test
    void makeHero_checkIntelligenceAttributeWhenCreated_expectedCorrectValues() {
        mage.makeHero();
        assertEquals(8, mage.getIntelligence());

        ranger.makeHero();
        assertEquals(1, ranger.getIntelligence());

        rogue.makeHero();
        assertEquals(1, rogue.getIntelligence());

        warrior.makeHero();
        assertEquals(1, warrior.getIntelligence());
    }

    /** Tests for attributes after levelling up **/
    @org.junit.jupiter.api.Test
    void levelUp_checkVitalityAfterLevelUp_expectedIncreasedAttributes() {
        mage.makeHero();
        mage.levelUp();
        assertEquals(8, mage.getVitality());

        ranger.makeHero();
        ranger.levelUp();
        assertEquals(10, ranger.getVitality());

        rogue.makeHero();
        rogue.levelUp();
        assertEquals(11, rogue.getVitality());

        warrior.makeHero();
        warrior.levelUp();
        assertEquals(15, warrior.getVitality());

    }

    @org.junit.jupiter.api.Test
    void levelUp_checkStrengthAfterLevelUp_expectedIncreasedAttributes() {
        mage.makeHero();
        mage.levelUp();
        assertEquals(2, mage.getStrength());

        ranger.makeHero();
        ranger.levelUp();
        assertEquals(2, ranger.getStrength());

        rogue.makeHero();
        rogue.levelUp();
        assertEquals(3, rogue.getStrength());

        warrior.makeHero();
        warrior.levelUp();
        assertEquals(8, warrior.getStrength());

    }

    @org.junit.jupiter.api.Test
    void levelUp_checkDexterityAfterLevelUp_expectedIncreasedAttributes() {
        mage.makeHero();
        mage.levelUp();
        assertEquals(2, mage.getDexterity());

        ranger.makeHero();
        ranger.levelUp();
        assertEquals(12, ranger.getDexterity());

        rogue.makeHero();
        rogue.levelUp();
        assertEquals(10, rogue.getDexterity());

        warrior.makeHero();
        warrior.levelUp();
        assertEquals(4, warrior.getDexterity());

    }

    @org.junit.jupiter.api.Test
    void levelUp_checkIntelligenceAfterLevelUp_expectedIncreasedAttributes() {
        mage.makeHero();
        mage.levelUp();
        assertEquals(13, mage.getIntelligence());

        ranger.makeHero();
        ranger.levelUp();
        assertEquals(2, ranger.getIntelligence());

        rogue.makeHero();
        rogue.levelUp();
        assertEquals(2, rogue.getIntelligence());

        warrior.makeHero();
        warrior.levelUp();
        assertEquals(2, warrior.getIntelligence());

    }

    @org.junit.jupiter.api.Test
    void heroDPS_checkHeroDPSWithNoWeaponEquipped() {
        warrior.makeHero();
        double heroDPS = warrior.heroDPS();

        assertEquals((1+(5/100.0) * 1), heroDPS);

    }

    @org.junit.jupiter.api.Test
    void heroDPS_checkHeroDPSWithWeaponEquipped() throws InvalidArmorException, InvalidWeaponException {

        Item testWeapon = new Weapons();
        testWeapon.setName("common axe");
        testWeapon.setRequiredLevel(1);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.AXES);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(7.7);
        ((Weapons) testWeapon).setAttackSpeed(1.1);


        warrior.makeHero();
        warrior.equipItem(testWeapon);
        double heroDPS = warrior.heroDPS();

        assertEquals((1+(5/100.0) * (7.7*1.1)), heroDPS);

    }
    @org.junit.jupiter.api.Test
    void heroDPS_checkHeroDPSWithWeaponAndArmorEquipped() throws InvalidArmorException, InvalidWeaponException {

        Item testWeapon = new Weapons();
        testWeapon.setName("common axe");
        testWeapon.setRequiredLevel(1);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.AXES);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(7.7);
        ((Weapons) testWeapon).setAttackSpeed(1.1);

        Armor armor = new Armor();
        armor.setName("Common plate");
        armor.setRequiredLevel(1);
        armor.setArmorType(Armor.ArmorTypes.PLATE);
        armor.setSlot(Item.Slot.BODY);

        warrior.makeHero();
        warrior.equipItem(testWeapon);
        warrior.equipItem(armor);
        double heroDPS = warrior.heroDPS();

        assertEquals((1+((5+1)/100.0) * (7.7*1.1)), heroDPS);

    }




}