package com.company;

import com.company.HeroClasses.Mage;
import com.company.HeroClasses.Ranger;
import com.company.HeroClasses.Rogue;
import com.company.HeroClasses.Warrior;
import org.junit.jupiter.api.Assertions;
import util.InvalidArmorException;
import util.InvalidWeaponException;

import static org.junit.jupiter.api.Assertions.*;

class EquipmentTest {

    private final Hero mage = new Mage();
    private final Hero ranger = new Ranger();
    private final Hero rogue = new Rogue();
    private final Hero warrior = new Warrior();


    @org.junit.jupiter.api.Test
    void equipItem_CheckIfInvalidLevelForEquippingWeapon_InvalidWeaponExceptionShouldBeThrown() {
        Item testWeapon = new Weapons();
        testWeapon.setName("common axe");
        testWeapon.setRequiredLevel(2);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.AXES);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(10);
        ((Weapons) testWeapon).setAttackSpeed(2);

        warrior.makeHero();

        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(testWeapon);
        });
    }

    @org.junit.jupiter.api.Test
    void equipItem_CheckIfInvalidLevelForEquippingArmor_InvalidArmorExceptionShouldBeThrown() {
        Armor armor = new Armor();
        armor.setName("Common plate");
        armor.setRequiredLevel(2);
        armor.setArmorType(Armor.ArmorTypes.PLATE);
        armor.setSlot(Item.Slot.BODY);

        warrior.makeHero();

        Assertions.assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(armor);
        });
    }

    @org.junit.jupiter.api.Test
    void equipItem_CheckIfInvalidTypeOfWeapon_InvalidWeaponExceptionShouldBeThrown() {
        Item testWeapon = new Weapons();
        testWeapon.setName("common bow");
        testWeapon.setRequiredLevel(1);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.BOWS);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(10);
        ((Weapons) testWeapon).setAttackSpeed(2);

        warrior.makeHero();

        Assertions.assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(testWeapon);
        });
    }

    @org.junit.jupiter.api.Test
    void equipItem_CheckIfInvalidTypeOfArmor_InvalidArmorExceptionShouldBeThrown() {
        Armor armor = new Armor();
        armor.setName("Common cloth");
        armor.setRequiredLevel(1);
        armor.setArmorType(Armor.ArmorTypes.CLOTH);
        armor.setSlot(Item.Slot.BODY);

        warrior.makeHero();

        Assertions.assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(armor);
        });
    }

    @org.junit.jupiter.api.Test
    void equipItem_CheckIfWeaponValid_EquippedSuccessfully() throws InvalidArmorException, InvalidWeaponException {
        Item testWeapon = new Weapons();
        testWeapon.setName("common Axe");
        testWeapon.setRequiredLevel(1);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.AXES);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(10);
        ((Weapons) testWeapon).setAttackSpeed(2);

        warrior.makeHero();

        warrior.equipItem(testWeapon);

        //If of valid type and level the weapon, it gets put in the Hashmap. We can check for
        //that hashmap slot to see if it is equipped.
        assertTrue(warrior.getEquipment().get(testWeapon.getSlot()) != null );
    }

    @org.junit.jupiter.api.Test
    void equipItem_CheckIfArmorValid_EquippedSuccessfully() throws InvalidArmorException {
        Armor armor = new Armor();
        armor.setName("Common plate");
        armor.setRequiredLevel(1);
        armor.setArmorType(Armor.ArmorTypes.PLATE);
        armor.setSlot(Item.Slot.BODY);

        warrior.makeHero();

        try {
            warrior.equipItem(armor);
        } catch (InvalidWeaponException e) {
            e.printStackTrace();
        }

        //If of valid type and level the armor gets put in the Hashmap. We can check for
        //that hashmap slot to see if it is equipped.
        assertTrue(warrior.getEquipment().get(armor.getSlot()) != null );
    }



}