package com.company;

public abstract class Item {

    public String itemName;
    public int requiredLevel;
    public enum Slot {HEAD, BODY, LEGS, WEAPON};

    public Item(){

    }

    Slot slot;

    //Not used but another way of equipping items
    public abstract void equipItemTo(Hero hero) throws Exception;

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getItemName() {
        return itemName;
    }

    public void setName(String name) {
        this.itemName = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }
}




