package com.company;

import com.company.HeroClasses.Warrior;

public class Weapons extends Item {

    private double damage;
    private double attackSpeed;


    public enum WeaponTypes {AXES, BOWS, DAGGERS, HAMMERS, STAFFS, SWORDS, WANDS };
    WeaponTypes weaponType;



    //Not used
    public void equipItemTo(Hero hero) {
        if(hero.getName() == "Warrior" && getWeaponType() == WeaponTypes.BOWS) {
            System.out.println( hero.name + " warrior can't equip bows " + getWeaponType());
        } else if (hero.getName() == "Mage") {
            System.out.println(hero.name + " equipped " + getItemName());
        }

    }

    public double calculateDPS() {

        double dps;

        dps = getDamage()*getAttackSpeed();

        return dps;

    }



    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponTypes weaponType) {
        this.weaponType = weaponType;
    }
}
