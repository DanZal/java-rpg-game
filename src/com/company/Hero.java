package com.company;

import util.InvalidArmorException;
import util.InvalidWeaponException;

import java.util.HashMap;

public abstract class Hero extends Attributes{

    protected String name;
    protected int level;
    protected HashMap<Item.Slot, Item> equipment = new HashMap<>();

    public Hero () {

    }

    public abstract void makeHero();

    //EquipItem method is implemented here because it makes it easier to check if the
    //right hero is trying to equip an item
    public abstract void equipItem(Item item) throws InvalidWeaponException, InvalidArmorException;

    public abstract double heroDPS();

    public abstract void printHeroStats();

    public abstract void levelUp();

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HashMap<Item.Slot, Item> getEquipment() {
        return equipment;
    }

}
