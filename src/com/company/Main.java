package com.company;

import com.company.HeroClasses.Mage;
import com.company.HeroClasses.Ranger;
import com.company.HeroClasses.Rogue;
import com.company.HeroClasses.Warrior;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {


        Hero mage = new Mage();
        mage.makeHero();

        Hero warrior = new Warrior();
        warrior.makeHero();




        ArrayList<Hero> heroes = new ArrayList<>();
        heroes.add(new Mage());
        heroes.add(new Warrior());
        heroes.add(new Rogue());

        /*for (Hero hers : heroes) {
            hers.makeHero(hers);
            hers.levelUp(hers);
            hers.levelUp(hers);
        }*/

        Item testWeapon = new Weapons();
        testWeapon.setName("common sword");
        testWeapon.setRequiredLevel(1);
        ((Weapons) testWeapon).setWeaponType(Weapons.WeaponTypes.AXES);
        testWeapon.setSlot(Item.Slot.WEAPON);
        ((Weapons) testWeapon).setDamage(7);
        ((Weapons) testWeapon).setAttackSpeed(1.1);

        System.out.println("Weapon: " + testWeapon.getItemName()+ " Weapon type: " + ((Weapons) testWeapon).getWeaponType()
        + " Weapon slot: " + testWeapon.getSlot() + " Weapon DPS: " + ((Weapons) testWeapon).calculateDPS() + " Weapon is equipped in slot: " + testWeapon.getSlot());


        Armor armor = new Armor();
        armor.setName("cloth");
        armor.setRequiredLevel(1);
        armor.setArmorType(Armor.ArmorTypes.CLOTH);
        armor.setSlot(Item.Slot.BODY);


        //armor.equipItemTo(mage);

        mage.equipItem(armor);
        //mage.equipItem(testWeapon);



        if (mage.getEquipment().get(testWeapon.getSlot()) != null) {
            System.out.println(mage.getEquipment().get(testWeapon.getSlot()).itemName);
        }
        mage.printHeroStats();
        //warrior.heroDPS(testWeapon);

        warrior.heroDPS();
        warrior.equipItem(testWeapon);

        warrior.heroDPS();





    }


}
