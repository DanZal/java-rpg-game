package com.company.HeroClasses;

import com.company.Armor;
import com.company.Hero;
import com.company.Item;
import com.company.Weapons;
import util.InvalidArmorException;
import util.InvalidWeaponException;

import java.util.StringJoiner;

public class Mage extends Hero {

    private int levelupVitality = 3;
    private int levelupDexterity = 1;
    private int levelupStrength = 1;
    private int levelupIntelligence = 5;
    double heroDps;


    @Override
    public void makeHero() {

        this.setVitality(5);
        this.setDexterity(1);
        this.setStrength(1);
        this.setIntelligence(8);
        this.setName("Mage");
        this.setLevel(1);
        heroDPS();


    }

    @Override
    public void equipItem(Item item) {
        if (item instanceof Armor) {

            if (item.getRequiredLevel() > this.getLevel()) {
                try {
                    throw new InvalidArmorException(this.getName() + " is not high enough level to equip this " + ((Armor) item).getArmorType().toString().toLowerCase());
                } catch (InvalidArmorException e) {
                    e.printStackTrace();
                }
            }

            switch (((Armor) item).getArmorType()) {
                    case CLOTH:
                        equipment.put(item.getSlot(), item);
                        System.out.println(this.getName() + " is equipping... " + item.itemName);
                        this.setVitality(getVitality()+1);
                        this.setIntelligence(getIntelligence()+5);
                        break;
                    default:
                        try {
                            throw new InvalidArmorException(this.getName() + " can not equip " + ((Armor) item).getArmorType().toString().toLowerCase());
                        } catch (InvalidArmorException e) {
                            e.printStackTrace();
                        }
                }
        } else if (item instanceof Weapons ) {
            if (item.getRequiredLevel() > this.getLevel()) {
                try {
                    throw new InvalidWeaponException(this.getName() + " is not high enough level to equip this " + ((Weapons) item).getWeaponType().toString().toLowerCase());
                } catch (InvalidWeaponException e) {
                    e.printStackTrace();
                }
            }

            switch (((Weapons) item).getWeaponType()) {
                case STAFFS, WANDS:
                    equipment.put(item.getSlot(), item);
                    heroDPS();
                    System.out.println(this.getName() + " is equipping... " + item.itemName + " DPS is " + ((Weapons) item).calculateDPS());
                    break;
                default:
                    try {
                        throw new InvalidWeaponException(this.getName() + " can not equip " + ((Weapons) item).getWeaponType().toString().toLowerCase());
                    } catch (InvalidWeaponException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    @Override
    public double heroDPS() {
        int armor = 0;
        if(this.getEquipment().get(Item.Slot.BODY) != null) {
            armor +=1;
        } else if (this.getEquipment().get(Item.Slot.LEGS) != null) {
            armor +=1;
        } else if (this.getEquipment().get(Item.Slot.HEAD) != null) {
            armor +=1;
        }

        if (this.getEquipment().get(Item.Slot.WEAPON) == null) {
            heroDps= (1+(5+armor/100.0) * 1);
        }
        else {
            Item weapon = this.getEquipment().get(Item.Slot.WEAPON);
            heroDps = (1+((5+armor)/100.0) * (((Weapons) weapon).calculateDPS()));
        }
        return heroDps;

    }

    @Override
    public void printHeroStats() {
        StringJoiner joiner = new StringJoiner(" ");
        joiner.add("Hero stats: \n")
                .add("Name: " + this.getName()+"\n")
                .add("Level: " + this.getLevel()+"\n")
                .add("Strength: " + this.getStrength()+"\n")
                .add("Dexterity: " + this.getDexterity()+"\n")
                .add("Intelligence: " + this.getIntelligence()+"\n")
                .add("Vitality: " + this.getVitality()+"\n")
                .add("DPS: " + heroDps+"\n");
        System.out.print(joiner);
    }


    @Override
    public void levelUp() {
        this.setVitality(getVitality()+levelupVitality);
        this.setDexterity(getDexterity()+levelupDexterity);
        this.setStrength(getStrength()+levelupStrength);
        this.setIntelligence(getIntelligence()+levelupIntelligence);
        this.setLevel(getLevel()+1);

    }
}
