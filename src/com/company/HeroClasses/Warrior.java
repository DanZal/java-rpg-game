package com.company.HeroClasses;

import com.company.Armor;
import com.company.Hero;
import com.company.Item;
import com.company.Weapons;
import org.jetbrains.annotations.NotNull;
import util.InvalidArmorException;
import util.InvalidWeaponException;

import javax.management.Attribute;
import java.util.HashMap;
import java.util.StringJoiner;

public class Warrior extends Hero {

    //use final int for all levelup attributes
    private final int levelupVitality = 5;
    private int levelupDexterity = 2;
    private int levelupStrength = 3;
    private int levelupIntelligence = 1;
    private double heroDps;

    @Override
    public void makeHero() {
        this.setVitality(10);
        this.setDexterity(2);
        this.setStrength(5);
        this.setIntelligence(1);
        this.setName("Warrior");
        this.setLevel(1);
        heroDPS();
    }

    @Override
    public void equipItem(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (item instanceof Armor) {

            if (item.getRequiredLevel() > this.getLevel()) {
                throw new InvalidArmorException(this.getName() + " is not high enough level to equip this " + ((Armor) item).getArmorType().toString().toLowerCase());
            }

            switch (((Armor) item).getArmorType()) {
                case MAIL,PLATE:
                    equipment.put(item.getSlot(), item);
                    System.out.println(this.getName() + " is equipping... " + item.itemName);
                    this.setVitality(getVitality()+2);
                    this.setStrength(getStrength()+1);
                    break;
                default:
                    throw new InvalidArmorException(this.getName() + " can not equip " + ((Armor) item).getArmorType().toString().toLowerCase());
            }
        } else if (item instanceof Weapons ) {

            if (item.getRequiredLevel() > this.getLevel()) {
                throw new InvalidWeaponException(this.getName() + " is not high enough level to equip this " + ((Weapons) item).getWeaponType().toString().toLowerCase());
                }

            switch (((Weapons) item).getWeaponType()) {
                case AXES,HAMMERS,SWORDS:
                    equipment.put(item.getSlot(), item);
                    heroDPS();
                    System.out.println(this.getName() + " is equipping... " + item.itemName + " DPS is " + ((Weapons) item).calculateDPS());
                    break;
                default:
                    throw new InvalidWeaponException(this.getName() + " can not equip " + ((Weapons) item).getWeaponType().toString().toLowerCase());

                }
            }
        }



    @Override
    public double heroDPS() {
        int armor = 0;
        if(this.getEquipment().get(Item.Slot.BODY) != null) {
            armor +=1;
        } else if (this.getEquipment().get(Item.Slot.LEGS) != null) {
            armor +=1;
        } else if (this.getEquipment().get(Item.Slot.HEAD) != null) {
            armor +=1;
        }

        if (this.getEquipment().get(Item.Slot.WEAPON) == null) {
            heroDps= (1+(5+armor/100.0) * 1);
        }
        else {
            Item weapon = this.getEquipment().get(Item.Slot.WEAPON);
            heroDps = (1+((5+armor)/100.0) * (((Weapons) weapon).calculateDPS()));
        }
        return heroDps;
    }

    @Override
    public void printHeroStats() {
        StringJoiner joiner = new StringJoiner(" ");
        joiner.add("Hero stats: \n")
                .add("Name: " + this.getName()+"\n")
                .add("Level: " + this.getLevel()+"\n")
                .add("Strength: " + this.getStrength()+"\n")
                .add("Dexterity: " + this.getDexterity()+"\n")
                .add("Intelligence: " + this.getIntelligence()+"\n")
                .add("Vitality: " + this.getVitality()+"\n")
                .add("DPS: " + heroDps+"\n");
        System.out.print(joiner);
    }

    @Override
    public void levelUp() {
        this.setVitality(getVitality()+levelupVitality);
        this.setDexterity(getDexterity()+levelupDexterity);
        this.setStrength(getStrength()+levelupStrength);
        this.setIntelligence(getIntelligence()+levelupIntelligence);
        this.setLevel(getLevel()+1);
    }



}
