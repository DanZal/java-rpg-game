package com.company;

public class Armor extends Item{

    public enum ArmorTypes {CLOTH, LEATHER, MAIL, PLATE };
    ArmorTypes armorType;


    @Override
    public void equipItemTo(Hero hero) {

        if (hero.getName() == "Mage" && getArmorType() == ArmorTypes.CLOTH) {
            hero.setVitality(2);
            System.out.println(hero.getName() + " has equipped " + getItemName() + " vitality has increased to " + hero.getVitality());
        } else {
            System.out.println(hero.getName() + " cannot equip " + getItemName());
        }
    }




    public ArmorTypes getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorTypes armorType) {
        this.armorType = armorType;
    }
}
