# java-rpg-game

# Desciption:
Java RPG style console application.

# Hero classes:
- Mage
- Ranger
- Rogue
- Warrior


Each with their own set of attributes.

# Functionalities:
- Create a hero 
- Level up a hero
- Create Items (Armor, Weapons)
- Equip items to a hero
- Calucate DPS for weapon and overall DPS for Hero
- Print Hero stats


# Testing:
JUnit testing done with JUnit version 5.
All functionalites have been put through and passed the tests.


Application developed in Intellij IDEA with Java SDK 16.

